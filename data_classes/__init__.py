from typing import NewType

RecordId = NewType("RecordId", int)
