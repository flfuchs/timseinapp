from dataclasses import dataclass
from datetime import datetime
from enum import unique, Enum
from typing import Tuple

from data_classes import RecordId


@unique
class ObservedDirection(Enum):
    In = 1
    Out = -1


@dataclass(frozen=True)
class SingleCordonObservation:
    time_stamp: datetime
    direction: ObservedDirection


@dataclass(frozen=True)
class RecordedCordonObservations:
    id: RecordId
    records: Tuple[SingleCordonObservation, ...]
