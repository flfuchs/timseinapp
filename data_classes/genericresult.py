from __future__ import annotations

from dataclasses import dataclass
from typing import Generic, Optional, Type, TypeVar

ResultType = TypeVar("ResultType")
AnswerType = TypeVar("AnswerType")
T = TypeVar("T", bound="Parent")


@dataclass(frozen=True)
class GenericResult(Generic[ResultType, AnswerType]):
    value: Optional[ResultType]
    answer: Optional[AnswerType]

    @property
    def success(self) -> bool:
        return self.value is not None

    @classmethod
    def from_result(cls: Type[T], result: ResultType) -> T:
        return cls(result, None)

    @classmethod
    def from_answer(cls: Type[T], error: AnswerType) -> T:
        return cls(None, error)

    @property
    def value_or_error(self) -> ResultType:
        if self.success:
            return self.value  # noqa
        else:
            raise ValueError("tried to get a value from failed result")

    @property
    def answer_or_error(self) -> AnswerType:
        if not self.success:
            return self.answer  # noqa
        else:
            raise ValueError("tried to get answers from successful result")
