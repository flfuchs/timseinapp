from dataclasses import dataclass
from datetime import datetime
from enum import unique, Enum

from data_classes import RecordId


@unique
class DetectedCordonEvent(Enum):
    passed_cordon = "passed"
    entered_cordon = "entered_cordon"
    left_cordon = "left_cordon"


@dataclass
class EvaluatedCordonDetection:
    id: RecordId
    time_stamp: datetime
    event: DetectedCordonEvent
