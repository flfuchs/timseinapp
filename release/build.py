import os


def build_from_cmd_line():
    os.chdir("..//")
    print(os.getcwd())
    arguments = (
        "pyinstaller --noconfirm --onefile --console "
        "--icon ./release/227277687_245209897489192_556367984049834573_n.ico "
        '--name "timSeinApp" '
        '--hidden-import "dash" -'
        '-hidden-import "plotly" '
        "--collect-submodules "
        '"dash" '
        '--collect-submodules "plotly" '
        '--collect-data "dash" '
        '--collect-data "plotly" '
        '--copy-metadata "dash" '
        '--copy-metadata "plotly" '
        '--recursive-copy-metadata "plotly" '
        '--recursive-copy-metadata "dash"  '
        '"./main.py" '
    )
    os.system(arguments)


if __name__ == "__main__":
    build_from_cmd_line()
