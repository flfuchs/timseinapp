from dashboard.app_assembler import create_and_start_app_with_dashboard

if __name__ == "__main__":
    create_and_start_app_with_dashboard(debug=True)
