from setuptools import setup

setup(
    name="timSeinApp",
    version="0.0.1",
    packages=["dashboard", "data_classes", "data_handling"],
    url="",
    license="MIT",
    author="ffu",
    author_email="flfuchs@student.ethz.ch",
    description="A proof of concept",
)
