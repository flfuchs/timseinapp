from dash import dcc, html

from dashboard.id_provider import DashboardIds


def _create_upload_element() -> dcc.Upload:
    return dcc.Upload(
        id=DashboardIds.UPLOAD.value,
        children=html.Div(["Drag and Drop or ", html.A("Select Files")]),
        style={
            "width": "100%",
            "height": "60px",
            "lineHeight": "60px",
            "borderWidth": "1px",
            "borderStyle": "dashed",
            "borderRadius": "5px",
            "textAlign": "center",
            "margin": "10px",
        },
        # Allow multiple files to be uploaded
        multiple=False,
    )


def _create_scatter_graph() -> dcc.Graph:
    return dcc.Graph(id=DashboardIds.SCATTER_PLOT.value)


def _create_passing_duration_slider():
    return dcc.Slider(
        id=DashboardIds.PASSING_DURATION_SLIDER.value,
        min=0.1,
        max=10,
        step=0.1,
        marks={0.1: "0.1 minutes", 5: "5 minutes", 10: "10 minutes"},
        value=2,
    )
