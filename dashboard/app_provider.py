import webbrowser

from dash import Dash, html

from dashboard.layout_provider import _create_upload_element, _create_scatter_graph, _create_passing_duration_slider

external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(
    [
        _create_upload_element(),
        _create_scatter_graph(),
        html.P("Time it takes to pass the cordon"),
        _create_passing_duration_slider(),
    ]
)
