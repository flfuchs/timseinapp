import base64
import datetime
import io

import pandas as pd
from dash import html, Output, Input, State
from plotly import graph_objects as go

from _hotfix import hotfix_to_convert_records
from dashboard.app_provider import app
from dashboard.id_provider import DashboardIds
from data_classes.genericresult import GenericResult
from data_handling.parsing import parse_cordon_counting_csv
from data_handling.processing import process_with_variable_passing_duration_and_sort_events_by_timestamp
from data_handling.visualizing import plot_events_vs_timestamps

app = app


def parse_contents(contents, filename, date) -> html.Div:
    return html.Div(
        [
            html.H5(filename),
            html.H6(datetime.datetime.fromtimestamp(date)),
            # HTML images accept base64 encoded strings in the same format
            # that is supplied by the upload
            html.Img(src=contents),
            html.Hr(),
            html.Div("Raw Content"),
            html.Pre(contents[0:200] + "...", style={"whiteSpace": "pre-wrap", "wordBreak": "break-all"}),
        ]
    )


def parse_csv_or_xlsx_from_file_contents(contents, filename) -> GenericResult[pd.DataFrame, str]:
    content_type, content_string = contents.split(",")
    decoded = base64.b64decode(content_string)
    try:
        if "csv" in filename:
            # Assume that the user uploaded a CSV file
            return GenericResult.from_result(pd.read_csv(io.StringIO(decoded.decode("utf-8"))))
        elif "xls" in filename:
            # Assume that the user uploaded an excel file
            return GenericResult.from_result(pd.read_excel(io.BytesIO(decoded)))
    except Exception as e:
        print(e)
        return GenericResult.from_answer(f"There was an error processing this file:\n {e}")


@app.callback(
    Output(DashboardIds.SCATTER_PLOT.value, "figure"),
    Input(DashboardIds.UPLOAD.value, "contents"),
    Input(DashboardIds.PASSING_DURATION_SLIDER.value, "value"),
    State(DashboardIds.UPLOAD.value, "filename"),
    State(DashboardIds.UPLOAD.value, "last_modified"),
)
def update_scatter_plot(
        upload_contents, passing_duration_slider_value: float, filename: str, modification_date: datetime.datetime
) -> go.Figure:
    if upload_contents is not None:
        parsing_result = parse_csv_or_xlsx_from_file_contents(upload_contents, filename)
        if parsing_result.success:
            raw_counts = parse_cordon_counting_csv(parsing_result.value)
        else:
            return go.Figure()
        parsed_counts = hotfix_to_convert_records(raw_counts)
        estimated_passing_duration = datetime.timedelta(minutes=passing_duration_slider_value)
        detected_events = process_with_variable_passing_duration_and_sort_events_by_timestamp(
            parsed_counts, estimated_passing_duration
        )
        return plot_events_vs_timestamps(detected_events)
    else:
        return go.Figure()
