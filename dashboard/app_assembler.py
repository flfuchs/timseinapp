import webbrowser
from threading import Timer

from dashboard.callback_provider import app


def create_and_start_app_with_dashboard(debug=False) -> None:
    port = 8081
    app.run_server(debug=debug, port=port)
    if not debug:
        Timer(0.001, webbrowser.open_new(f"http://localhost:{port}")).start()  # noqa
