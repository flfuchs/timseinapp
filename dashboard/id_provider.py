from enum import unique, Enum


@unique
class DashboardIds(Enum):
    UPLOAD = "upload-csv-with-cordon-counting"
    SCATTER_PLOT = "scatter"
    PASSING_DURATION_SLIDER = "passing-duration-slider"


DASHBOARD_IDS = DashboardIds
