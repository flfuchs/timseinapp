from typing import Dict, Any, List

from data_classes.observations import RecordedCordonObservations, SingleCordonObservation


# TODO: Remove me!!!!
def hotfix_to_convert_records(all_detections: Dict[Any, List[tuple]]) -> Dict[Any, RecordedCordonObservations]:
    converted_detections = dict()
    for key, values in all_detections.items():
        converted_values = []
        for value in values:
            converted_values.append(SingleCordonObservation(value[0], value[1]))

        converted_detections[key] = RecordedCordonObservations(key, tuple(converted_values))
    return converted_detections
