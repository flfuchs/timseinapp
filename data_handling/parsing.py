from datetime import datetime


def parse_cordon_counting_csv(df):
    # parse dataframe rows and bundle them to a dict with lists of records
    # each record is a tuple of the following sequence
    # time: a datetime object
    # direction: a string indicating the direection of the observation
    # place: the place where the observation was taken
    recorded_detections_by_id = dict()
    for index, row in df.iterrows():
        try:
            record_id = row["licplate"]
            if record_id not in recorded_detections_by_id.keys():
                recorded_detections_by_id[record_id] = []
            recorded_detections_by_id[record_id].append(
                (datetime.strptime(row["time"], "%d.%m.%Y %H:%M:%S"), row["directcordon"], row["place"])
            )
        except Exception as e:
            # we just ignore wrong records
            print(e)

    return recorded_detections_by_id
