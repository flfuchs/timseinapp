from typing import Tuple

import plotly.graph_objects as go

from data_classes.detections import DetectedCordonEvent, EvaluatedCordonDetection

_event_to_y_axis_mapping = {
    DetectedCordonEvent.entered_cordon: 1,
    DetectedCordonEvent.passed_cordon: 0,
    DetectedCordonEvent.left_cordon: -1,
}


def _transform_event_to_y(event: DetectedCordonEvent) -> int:
    return _event_to_y_axis_mapping[event]


def _create_empty_figure() -> go.Figure():
    return go.Figure()


def plot_events_vs_timestamps(evaluated_detections: Tuple[EvaluatedCordonDetection, ...]) -> go.Figure:
    figure = _create_empty_figure()
    figure.add_scatter(
        x=[d.time_stamp for d in evaluated_detections],
        y=[_transform_event_to_y(d.event) for d in evaluated_detections],
        mode="markers",
        hovertext=[f"detection_id:{d.id}" for d in evaluated_detections],
    )
    figure.update_layout(
        yaxis=dict(
            tickmode="array",
            tickvals=tuple(_event_to_y_axis_mapping.values()),
            ticktext=tuple(k.value for k in _event_to_y_axis_mapping.keys()),
        )
    )
    return figure
