import itertools
from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import timedelta
from typing import Tuple, Dict

from data_classes import RecordId
from data_classes.detections import DetectedCordonEvent, EvaluatedCordonDetection
from data_classes.observations import ObservedDirection, SingleCordonObservation, RecordedCordonObservations


def pairwise(iterable):
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


@dataclass(frozen=True)
class _ProcessorValues:
    time_span_to_assume_passing: timedelta


class ProcessorABC(ABC):
    @abstractmethod
    def process(self, records: RecordedCordonObservations, record_id: RecordId) -> EvaluatedCordonDetection:
        pass


@dataclass(frozen=True)
class _SimpleCordonRecordedObservationsProcessor(ProcessorABC):
    processor_values: _ProcessorValues

    def process(
        self, detections: RecordedCordonObservations, record_id: RecordId
    ) -> Tuple[EvaluatedCordonDetection, ...]:
        sorted_detections = sorted(detections.records, key=lambda d: d.time_stamp)
        non_passing_detections = set(sorted_detections)
        non_sorted_evaluated_cordon_detections = []
        for preceding_record, succeeding_record in pairwise(sorted_detections):
            preceding_record: SingleCordonObservation
            succeeding_record: SingleCordonObservation
            time_delta = succeeding_record.time_stamp - preceding_record.time_stamp
            if (
                time_delta <= self.processor_values.time_span_to_assume_passing
                and preceding_record.direction == ObservedDirection.In
                and succeeding_record.direction == ObservedDirection.Out
            ):
                non_sorted_evaluated_cordon_detections.append(
                    EvaluatedCordonDetection(
                        record_id, preceding_record.time_stamp + time_delta * 0.5, DetectedCordonEvent.passed_cordon
                    )
                )
                non_passing_detections.remove(preceding_record)
                non_passing_detections.remove(succeeding_record)

        for non_passing_detection in non_passing_detections:
            if non_passing_detection.direction == ObservedDirection.In:
                non_sorted_evaluated_cordon_detections.append(
                    EvaluatedCordonDetection(
                        record_id, non_passing_detection.time_stamp, DetectedCordonEvent.entered_cordon
                    )
                )
            else:
                non_sorted_evaluated_cordon_detections.append(
                    EvaluatedCordonDetection(
                        record_id, non_passing_detection.time_stamp, DetectedCordonEvent.left_cordon
                    )
                )
        return tuple(sorted(non_sorted_evaluated_cordon_detections, key=lambda x: x.time_stamp))


def _process_all_detections_and_sort_them_by_timestamp(
    all_detections: Dict[RecordId, RecordedCordonObservations], processor_values: _ProcessorValues
) -> Tuple[EvaluatedCordonDetection, ...]:
    processor = _SimpleCordonRecordedObservationsProcessor(processor_values)
    non_sorted_processed_records = [
        processor.process(records, record_id) for record_id, records in all_detections.items()
    ]
    return tuple(sorted(itertools.chain.from_iterable(non_sorted_processed_records), key=lambda x: x.time_stamp))


def process_with_default_processor_values_and_sort_events_by_timestamp(
    all_detections: Dict[RecordId, RecordedCordonObservations]
) -> Tuple[EvaluatedCordonDetection, ...]:
    default_processor_values = _ProcessorValues(timedelta(seconds=120))
    return _process_all_detections_and_sort_them_by_timestamp(all_detections, default_processor_values)


def process_with_variable_passing_duration_and_sort_events_by_timestamp(
    all_detections: Dict[RecordId, RecordedCordonObservations], passing_duration: timedelta,
) -> Tuple[EvaluatedCordonDetection, ...]:
    default_processor_values = _ProcessorValues(passing_duration)
    return _process_all_detections_and_sort_them_by_timestamp(all_detections, default_processor_values)
